export type Writer = {
    id: number;
    name: string;
    is_editor: boolean;
};

export type DashboardWriter = {
    id: number;
    name: string;
    total_articles: number;
    total_articles_last_30: number;
}

export type Query<T> = {
    results: T[],
    count: number,
    prev: string | null,
    next: string | null,
}

export type Article = {
    id: number;
    created: string;
    modified: string;
    status: ArticleStatus;
    title: string;
    content: string;
    written_by: Writer;
}

export enum ArticleStatus {
    Pending = 'PENDING',
    Accepted = 'ACCEPTED',
    Rejected = 'REJECTED',
}
