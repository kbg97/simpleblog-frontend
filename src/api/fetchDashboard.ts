import { AxiosResponse } from "axios";
import {DashboardWriter, Query} from "types";
import { my_app } from "./client";

export default async (url: string | null = null): Promise<AxiosResponse<Query<DashboardWriter>>> => {
    return await my_app.get<Query<DashboardWriter>>(url ? `${url}` : '/v1/');
};
