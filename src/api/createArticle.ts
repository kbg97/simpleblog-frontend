import { my_app } from "./client";
import {AxiosResponse} from "axios";
import {Article} from "types";

export default async (title: string, content: string): Promise<AxiosResponse<Article>> => {
    const getUserFormData = new FormData();
    getUserFormData.append("title", title);
    getUserFormData.append("content", content);
    return await my_app.post<Article>('/v1/article/', getUserFormData);
};
