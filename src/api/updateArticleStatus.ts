import { my_app } from "./client";
import {AxiosResponse} from "axios";
import {Article, ArticleStatus} from "types";

export default async (id: number, status: ArticleStatus): Promise<AxiosResponse<Article>> => {
    const getUserFormData = new FormData();
    getUserFormData.append("status", status);
    getUserFormData.append("id", id.toString());
    return await my_app.put<Article>(`/v1/article-approval/`, getUserFormData);
};
