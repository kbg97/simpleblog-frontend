import { AxiosResponse } from "axios";
import {Article, Query} from "types";
import { my_app } from "./client";

export default async (url: string | null = null): Promise<AxiosResponse<Query<Article>>> => {
    return await my_app.get<Query<Article>>(url ? `${url}` : '/v1/articles-edited/');
};
