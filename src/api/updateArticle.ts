import { my_app } from "./client";
import {AxiosResponse} from "axios";
import {Article} from "types";

export default async (id: number | undefined, title: string, content: string): Promise<AxiosResponse<Article>> => {
    const getUserFormData = new FormData();
    getUserFormData.append("title", title);
    getUserFormData.append("content", content);
    return await my_app.put<Article>(`/v1/article/${id}/`, getUserFormData);
};
