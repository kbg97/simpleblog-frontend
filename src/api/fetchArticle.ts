import { my_app } from "./client";
import {AxiosResponse} from "axios";
import {Article} from "types";

export default async (id: number): Promise<AxiosResponse<Article>> => {
    return await my_app.get<Article>(`/v1/article/${id}/`);
};
