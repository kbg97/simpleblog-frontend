import axios from "axios";

export const URL = "http://localhost:3000";
export const API_URL = "https://simpleblog-api-interview.herokuapp.com";
export const my_app = axios.create({baseURL: API_URL});
