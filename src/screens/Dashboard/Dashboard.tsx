import React, {Component} from "react";
import {DashboardWriter} from "types";
import fetchDashboard from "api/fetchDashboard";
import {PaginationControl} from "components/PaginationControl";
import {DashboardTable} from "../../components/DashboardTable";

type DashboardState = {
    selectedPage: string | null;
    nextPage: string | null;
    prevPage: string | null;
    rows: DashboardWriter[];
    count: number;
}

export class Dashboard extends Component<unknown, DashboardState> {
    constructor(props: unknown) {
        super(props);

        this.state = {
            selectedPage: null,
            nextPage: null,
            prevPage: null,
            rows: [],
            count: 0,
        }
    }

    componentDidMount(): void {
        fetchDashboard().then(result => {
            this.setState({
                rows: result?.data?.results,
                nextPage: result?.data?.next,
                prevPage: result?.data?.prev,
                count: result?.data?.count
            })
        }).catch(error => console.log(error));
    }

    onNext = (): void => {
        fetchDashboard(this.state.nextPage).then(result => {
            this.setState({
                selectedPage: this.state.nextPage,
                rows: result?.data?.results,
                nextPage: result?.data?.next,
                prevPage: result?.data?.prev,
                count: result?.data?.count
            })
        }).catch(error => console.log(error));
    }

    onPrev = (): void => {
        fetchDashboard(this.state.prevPage).then(result => {
            this.setState({
                selectedPage: this.state.prevPage,
                rows: result?.data?.results,
                nextPage: result?.data?.next,
                prevPage: result?.data?.prev,
                count: result?.data?.count
            })
        }).catch(error => console.log(error));
    }

    render() {
        return (
            <div className="container" style={{height: '80vh'}}>
                <DashboardTable rows={this.state.rows} />
                <PaginationControl
                    onNext={this.onNext}
                    onPrev={this.onPrev}
                    hasResults={!!this.state.count}
                    totalCount={this.state.count}
                    pageSize={20}
                    isNextEnabled={!!this.state.nextPage}
                    isPrevEnabled={!!this.state.prevPage}/>
            </div>
        );
    }
}
