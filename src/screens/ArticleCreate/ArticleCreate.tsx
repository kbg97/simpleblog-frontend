import React, {Component} from "react";
import {ArticleForm} from "../../components/ArticleForm";
import createArticle from "../../api/createArticle";
import {formatError} from "../../services/utils/formaters";

type ArticleCreateState = {
    success: boolean;
    error: string | null;
}

export class ArticleCreate extends Component<unknown, ArticleCreateState> {
    constructor(props: unknown) {
        super(props);

        this.state = {
            success: false,
            error: null,
        };
    }

    onSubmit = (title: string, content: string): void => {
        createArticle(title, content).then(() => {
            this.setState({success: true, error: null})
        }).catch(error => {
            this.setState({success: false, error: formatError(error?.response?.data)})
        });
    }

    render() {
        return (
            <div className="container mt-5" style={{height: '80vh'}}>
                <div className="d-flex flex-column justify-content-around w-100 h-75">
                    <h1 className="display-4 mx-auto">Create Article</h1>
                    <ArticleForm
                        onSubmit={this.onSubmit}
                        error={this.state.error}
                        success={this.state.success}/>
                </div>
            </div>
        );
    }
}
