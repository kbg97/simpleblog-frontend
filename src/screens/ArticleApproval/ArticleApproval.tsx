import React, {Component} from "react";
import {Article, ArticleStatus} from "types";
import {PaginationControl} from "components/PaginationControl";
import {ArticleList} from "components/ArticleList";
import fetchPendingArticles from "api/fetchPendingArticles";
import updateArticleStatus from "../../api/updateArticleStatus";

type ArticleApprovalState = {
    selectedPage: string | null;
    nextPage: string | null;
    prevPage: string | null;
    rows: Article[];
    count: number;
}

export class ArticleApproval extends Component<unknown, ArticleApprovalState> {
    constructor(props: unknown) {
        super(props);

        this.state = {
            selectedPage: null,
            nextPage: null,
            prevPage: null,
            rows: [],
            count: 0,
        }
    }

    componentDidMount(): void {
        fetchPendingArticles().then(result => {
            this.setState({
                rows: result?.data?.results,
                nextPage: result?.data?.next,
                prevPage: result?.data?.prev,
                count: result?.data?.count
            })
        }).catch(error => console.log(error));
    }

    onNext = (): void => {
        fetchPendingArticles(this.state.nextPage).then(result => {
            this.setState({
                selectedPage: this.state.nextPage,
                rows: result?.data?.results,
                nextPage: result?.data?.next,
                prevPage: result?.data?.prev,
                count: result?.data?.count
            })
        }).catch(error => console.log(error));
    }

    onPrev = (): void => {
        fetchPendingArticles(this.state.prevPage).then(result => {
            this.setState({
                selectedPage: this.state.prevPage,
                rows: result?.data?.results,
                nextPage: result?.data?.next,
                prevPage: result?.data?.prev,
                count: result?.data?.count
            })
        }).catch(error => console.log(error));
    }

    onApprove = (id: number): void => {
        updateArticleStatus(id, ArticleStatus.Accepted).then(() => {
            const newRows = this.state.rows.filter(article => article.id !== id);
            this.setState({
                rows: newRows,
                count: this.state.count - 1,
            })
        }).catch(error => console.log(error));
    }

    onReject = (id: number): void => {
        updateArticleStatus(id, ArticleStatus.Rejected).then(() => {
            const newRows = this.state.rows.filter(article => article.id !== id);
            this.setState({
                rows: newRows,
                count: this.state.count - 1,
            })
        }).catch(error => console.log(error));
    }

    render() {
        return (
            <div className="container mt-3" style={{height: '80vh'}}>
                <ArticleList
                    rows={this.state.rows}
                    onApprove={this.onApprove}
                    onReject={this.onReject}/>
                <PaginationControl
                    onNext={this.onNext}
                    onPrev={this.onPrev}
                    hasResults={!!this.state.count}
                    totalCount={this.state.count}
                    pageSize={20}
                    isNextEnabled={!!this.state.nextPage}
                    isPrevEnabled={!!this.state.prevPage}/>
            </div>
        );
    }
}
