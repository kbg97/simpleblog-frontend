import React, {Component} from "react";
import {ArticleForm} from "../../components/ArticleForm";
import {formatError} from "../../services/utils/formaters";
import {StatusBadge} from "../../components/StatusBadge";
import updateArticle from "../../api/updateArticle";
import {Article} from "types";
import fetchArticle from "../../api/fetchArticle";
import { withRouter, RouteComponentProps } from "react-router-dom";

type ArticleDetailRouteParams = {
    articleId: string;
}

type ArticleDetailState = {
    success: boolean;
    error: string | null;
    article: Article | null;
}

class ArticleDetail extends Component<RouteComponentProps<ArticleDetailRouteParams>, ArticleDetailState> {
    constructor(props: RouteComponentProps<ArticleDetailRouteParams>) {
        super(props);

        this.state = {
            success: false,
            error: null,
            article: null,
        };
    }

    componentDidMount() {
        fetchArticle(parseInt(this.props.match.params?.articleId)).then((response) => {
            this.setState({article: response?.data})
        }).catch(error => {
            this.setState({success: false, error: formatError(error?.response?.data)})
        });
    }

    shouldComponentUpdate(nextProps: Readonly<RouteComponentProps<ArticleDetailRouteParams>>): boolean {
        if (nextProps.match.params?.articleId !== this.props.match.params?.articleId) {
            fetchArticle(parseInt(nextProps.match.params?.articleId)).then((response) => {
                this.setState({article: response?.data})
            }).catch(error => {
                this.setState({success: false, error: formatError(error?.response?.data)})
            });
        }

        return true;
    }

    onSubmit = (title: string, content: string): void => {
        updateArticle(this.state.article?.id, title, content).then((response) => {
            this.setState({success: true, error: null})
        }).catch(error => {
            this.setState({success: false, error: formatError(error?.response?.data)})
        });
    }

    render() {
        return (
            <div className="container mt-5" style={{height: '80vh'}}>
                <div className="d-flex flex-column justify-content-around w-100 h-75">
                    <div className="d-flex flex-row align-items-center justify-content-center">
                        <h1 className="display-4 mr-3">View Article</h1>
                        <StatusBadge status={this.state.article?.status}/>
                    </div>
                    <ArticleForm
                        noClear
                        onSubmit={this.onSubmit}
                        error={this.state.error}
                        success={this.state.success}
                        title={this.state.article?.title}
                        content={this.state.article?.content}/>
                </div>
            </div>
        );
    }
}

export default withRouter(ArticleDetail);
