import React, { FC } from "react";

export const NotFound: FC = () => (
    <div className="row align-items-center justify-content-center" style={{ height: '100vh' }}>
        <div className="col-auto">
            <h1>We are terribly sorry, but the page you are looking for was not found</h1>
        </div>
    </div>
);
