import React, {Component} from "react";
import { AuthenticationContext } from "contexts";

type LoginState = {
    username: string;
    password: string;
}

export class Login extends Component<unknown, LoginState> {
    static contextType = AuthenticationContext;

    constructor(props: unknown) {
        super(props);

        this.state = {
            username: '',
            password: '',
        }
    }

    onChange = (key: string, value: string): void => {
        this.setState(prevState => ({
            ...prevState,
            [key]: value,
        }))
    }

    onSubmit = (): void => {
        this.context.login(this.state.username, this.state.password);
    }

    render() {
        return (
            <div className="container" style={{height: '100vh'}}>
                <div className="row h-75 align-items-center justify-content-center w-50 mx-auto">
                        <h1 className="display-4">Login</h1>
                        <div className="w-100 column align-items-center justify-content-center">
                            <div className="form-group">
                                <input
                                    type="text"
                                    className="form-control"
                                    value={this.state.username}
                                    onChange={(event) =>
                                        this.onChange('username', event.target.value)}
                                    placeholder="Enter username"/>
                            </div>
                            <div className="form-group">
                                <input
                                    type="password"
                                    className="form-control"
                                    value={this.state.password}
                                    onChange={(event) =>
                                        this.onChange('password', event.target.value)}
                                    placeholder="Password"/>
                            </div>
                            <button
                                type="button"
                                className="btn btn-primary w-100"
                                onClick={() => this.onSubmit()}>
                                Submit
                            </button>
                        </div>
                </div>
            </div>
        );
    }
}
