import React, {Component} from "react";
import {Article} from "types";
import {PaginationControl} from "components/PaginationControl";
import {ArticleList} from "components/ArticleList";
import fetchEditedArticles from "../../api/fetchEditedArticles";

type ArticlesEditedState = {
    selectedPage: string | null;
    nextPage: string | null;
    prevPage: string | null;
    rows: Article[];
    count: number;
}

export class ArticlesEdited extends Component<unknown, ArticlesEditedState> {
    constructor(props: unknown) {
        super(props);

        this.state = {
            selectedPage: null,
            nextPage: null,
            prevPage: null,
            rows: [],
            count: 0,
        }
    }

    componentDidMount(): void {
        fetchEditedArticles().then(result => {
            this.setState({
                rows: result?.data?.results,
                nextPage: result?.data?.next,
                prevPage: result?.data?.prev,
                count: result?.data?.count
            })
        }).catch(error => console.log(error));
    }

    onNext = (): void => {
        fetchEditedArticles(this.state.nextPage).then(result => {
            this.setState({
                selectedPage: this.state.nextPage,
                rows: result?.data?.results,
                nextPage: result?.data?.next,
                prevPage: result?.data?.prev,
                count: result?.data?.count
            })
        }).catch(error => console.log(error));
    }

    onPrev = (): void => {
        fetchEditedArticles(this.state.prevPage).then(result => {
            this.setState({
                selectedPage: this.state.prevPage,
                rows: result?.data?.results,
                nextPage: result?.data?.next,
                prevPage: result?.data?.prev,
                count: result?.data?.count
            })
        }).catch(error => console.log(error));
    }

    render() {
        return (
            <div className="container mt-3" style={{height: '80vh'}}>
                <ArticleList
                    rows={this.state.rows}/>
                <PaginationControl
                    onNext={this.onNext}
                    onPrev={this.onPrev}
                    hasResults={!!this.state.count}
                    totalCount={this.state.count}
                    pageSize={20}
                    isNextEnabled={!!this.state.nextPage}
                    isPrevEnabled={!!this.state.prevPage}/>
            </div>
        );
    }
}
