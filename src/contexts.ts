import React from "react";

const authenticationDefaults = {
    isLoggedIn: null,
    isEditor: false,
    login: () => {},
    logout: () => {},
};

export type AuthenticationState = {
    isLoggedIn: boolean | null;
    isEditor: boolean;
    login?: (username: string, password:string) => void,
    logout?: () => void,
}

export const AuthenticationContext = React.createContext<AuthenticationState>(authenticationDefaults);
