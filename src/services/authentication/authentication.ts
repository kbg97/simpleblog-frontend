import { APP_USER, REFRESH_USER } from "./constants";
import { my_app } from "../../api/client";
import { Writer } from "types";
import { AxiosResponse } from "axios";

const CLIENT_ID = 'DPag9raumoDr128ELEEU1vaMrVbvFWWl7gjAmWxO';

const refreshToken = (): Promise<any> => {
    const currUser = JSON.parse(localStorage.getItem(APP_USER) || 'null');
    const refreshToken = JSON.parse(localStorage.getItem(REFRESH_USER) || 'null');
    const getUserFormData = new FormData();
    getUserFormData.append("grant_type", "refresh_token");
    getUserFormData.append("client_id", CLIENT_ID);
    getUserFormData.append("refresh_token", refreshToken);
    return new Promise((resolve, reject) => {
        my_app.post(`/o/token/`, getUserFormData, {
                headers: {
                    Authorization: `Bearer ${currUser}`
                }
            })
            .then(async response => {
                resolve(response);
            })
            .catch(error => {
                reject(error);
            });
    });
};

export const getUser = (): Promise<AxiosResponse<Writer> | null> => {
    const currUser = JSON.parse(localStorage.getItem(APP_USER) || 'null');

    if (!currUser) {
        return Promise.resolve(null);
    }
    my_app.defaults.headers.common["Authorization"] =
        "Bearer " + currUser;
    my_app.interceptors.response.use(
        response => {
            return response;
        },
        error => {
            if (error.response.status === 401){
                return refreshToken()
                    .then((response: any) => {
                        const config = error.config;
                        config.headers.Authorization =
                            "Bearer " + response.data.access_token;
                        return new Promise((resolve, reject) => {
                            my_app
                                .request(config)
                                .then(response => { resolve(response); })
                                .catch(error => { reject(error); });
                        });
                    })
                    .catch(() => { logout(false); });
                    }
            else if (error.response.status === 400 || error.response.status === 404) {
                return new Promise((resolve, reject) => { reject(error); });
            }

            logout(true);
            return new Promise((resolve, reject) => {
                reject(error);
            });
        }
    );
    return my_app.get<Writer>("/v1/profile/").catch(error => {
        logout(false);
        throw error;
    });
};

export const login = (username: string, password:string): Promise<AxiosResponse<Writer> | null> => {
    const loginFormData = new FormData();
    loginFormData.append("grant_type", "password");
    loginFormData.append("username", username);
    loginFormData.append("password", password);
    loginFormData.append("client_id", CLIENT_ID);
    return new Promise((resolve, reject) => {
        my_app.post(`/o/token/`, loginFormData)
            .then(async response => {
                localStorage.setItem(APP_USER, JSON.stringify(response.data.access_token));
                localStorage.setItem(REFRESH_USER, JSON.stringify(response.data.refresh_token));
                getUser()
                    .then(response => { resolve(response); })
                    .catch(error => { reject(error); });
            })
            .catch(error => { reject(error); });
    });
};

export const logout = (shouldRevoke: boolean) => {
    const currUser = JSON.parse(localStorage.getItem(APP_USER) || 'null');
    if (currUser && shouldRevoke) {
        const getUserFormData = new FormData();
        getUserFormData.append("token", currUser);
        getUserFormData.append("client_id", CLIENT_ID);
        new Promise(() => {
            my_app.post(`/o/revoke_token/`, getUserFormData)
        });
    }

    localStorage.removeItem(APP_USER);
    localStorage.removeItem(REFRESH_USER);
    return Promise.resolve();
};
