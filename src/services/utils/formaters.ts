export const formatError = (error: any): string => {
    if (error) {
        let errorMessage = '';
        Object.entries(error).forEach(([key, value]) => {
            const stringValue = typeof value === "string" ? value : (value as string[])?.[0]
            errorMessage += `${capitalizeFirstLetter(key)}: ${stringValue}\n`;
        })

        return errorMessage;
    }
    return '';
}

export const capitalizeFirstLetter = (string: string): string => {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
