import React, { Component } from 'react';

import './App.scss';
import { getUser, login, logout } from "services/authentication/authentication";
import { AppRouting } from "AppRouting";
import {AuthenticationState, AuthenticationContext} from "./contexts";

type AppState = {
  authenticationState: AuthenticationState;
};

class App extends Component<unknown, AppState> {
  constructor(props: unknown) {
    super(props);

    this.state = {
      authenticationState: {
        isLoggedIn: null,
        isEditor: false,
      }
    }
  }

  componentDidMount(): void {
    this.getUser();
  }

  getUser = async (): Promise<void> => {
    const user = await getUser();

    if (user?.data) {
      this.setState({ authenticationState: { isLoggedIn: true, isEditor: user?.data?.is_editor } });
    } else {
      this.setState({ authenticationState: { isLoggedIn: false, isEditor: false } });
    }
  }

  login = (username: string, password:string): void => {
    login(username, password).then((result) => {
      if (result?.data) {
        this.setState({ authenticationState: { isLoggedIn: true, isEditor: result?.data?.is_editor } });
      }
    })
  }

  logout = (): void => {
    logout(true).then(() =>
        this.setState({ authenticationState: { isLoggedIn: false, isEditor: false } }));
  }

  render() {
    return (
        <AuthenticationContext.Provider
            value={{ ...this.state.authenticationState, login: this.login, logout: this.logout }}>
          <AppRouting />
        </AuthenticationContext.Provider>
    );
  }
}

export default App;
