import React, { FC, useContext } from "react";
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { ROUTES } from "routes";
import { NotFound } from "screens/NotFound";
import { Login } from "screens/Login";
import { AuthenticationState, AuthenticationContext } from "contexts";
import { Dashboard } from "./screens/Dashboard";
import { ArticleCreate } from "./screens/ArticleCreate";
import { ArticleDetail } from "./screens/ArticleDetail";
import { ArticleApproval } from "./screens/ArticleApproval";
import { ArticlesEdited } from "./screens/ArticlesEdited";
import {Navbar} from "components/Navbar";

export const AppRouting: FC = () => {
    const {isLoggedIn, isEditor} = useContext<AuthenticationState>(AuthenticationContext);

    const renderLoggedInRoutes = () => (
        <>
            <Navbar />
            <Switch>
                <Redirect exact from="/" to={ROUTES.dashboard}/>
                <Redirect exact from="/login" to={ROUTES.dashboard}/>
                <Route path={ROUTES.dashboard} component={Dashboard}/>
                <Route exact path={ROUTES.article} component={ArticleCreate}/>
                <Route path={ROUTES.articleDetail} component={ArticleDetail}/>
                <Route path={ROUTES.error404} component={NotFound}/>
                {
                    isEditor ? renderEditorRoutes() : null
                }
                <Redirect to={ROUTES.error404} />
            </Switch>
        </>
    );

    const renderEditorRoutes = () => (
        <>
            <Route path={ROUTES.articleApproval} component={ArticleApproval}/>
            <Route path={ROUTES.articlesEdited} component={ArticlesEdited}/>
        </>
    );

    const renderLoggedOutRoutes = () =>(
        <Switch>
            <Route path={ROUTES.login} component={Login}/>
            <Redirect to={ROUTES.login}/>
        </Switch>

    );

    const renderRoutes = () => {
        switch (isLoggedIn) {
            case true:
                return renderLoggedInRoutes();
            case false:
                return renderLoggedOutRoutes();
            default:
                return null;
        }
    };

    return (
        <BrowserRouter>
            {renderRoutes()}
        </BrowserRouter>
    );
};
