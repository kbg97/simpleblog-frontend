import React, {Component} from "react";


type ArticleFormProps = {
    onSubmit: (title: string, content:string) => void;
    error: string | null;
    success: boolean;
    title?: string;
    content?: string;
    noClear?: boolean;
}

type ArticleFormState = {
    title: string;
    content: string;
}

export class ArticleForm extends Component<ArticleFormProps, ArticleFormState> {
    constructor(props: ArticleFormProps) {
        super(props);

        this.state = {
            title: '',
            content: '',
        }
    }

    componentDidMount() {
        this.setState({
            title: this.props.title || '',
            content: this.props.content || '',
        })
    }

    shouldComponentUpdate(nextProps: Readonly<ArticleFormProps>): boolean {
        if (nextProps.title !== this.props.title || nextProps.content !== this.props.content) {
            this.setState({
                title: nextProps.title || '',
                content: nextProps.content || '',
            })
        }

        return true;
    }

    onChange = (key: string, value: string): void => {
        this.setState(prevState => ({
            ...prevState,
            [key]: value,
        }))
    }

    onSubmit = (): void => {
        this.props.onSubmit(this.state.title, this.state.content);
        if (!this.props.noClear) {
            this.setState({
                title: '',
                content: '',
            })
        }
    }

    render() {
        return (
            <div className="d-flex flex-column align-items-center">
                <div className="form-group w-75">
                    <input
                        type="text"
                        className="form-control"
                        value={this.state.title}
                        onChange={(event) =>
                            this.onChange('title', event.target.value)}
                        placeholder="Title"/>
                </div>
                <div className="form-group w-75">
                    <textarea
                        style={{ maxHeight: 350, minHeight: 50, height: 100 }}
                        className="form-control"
                        value={this.state.content}
                        onChange={(event) =>
                            this.onChange('content', event.target.value)}
                        placeholder="Content"/>
                </div>
                <button
                    type="button"
                    className="btn btn-primary w-50"
                    onClick={() => this.onSubmit()}>
                    Submit
                </button>
                {
                    this.props.error ?
                        (
                            <div className="alert alert-danger" role="alert">
                                {this.props.error}
                            </div>
                        )
                        :
                        null
                }
                {
                    this.props.success ?
                        (
                            <div className="alert alert-success" role="alert">
                                Success!
                            </div>
                        )
                        :
                        null
                }
            </div>
        );
    }
}
