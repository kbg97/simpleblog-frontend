import React, {Component} from "react";

type ApproveRejectButtonPairProps = {
    onApprove: () => void;
    onReject: () => void;
};

export class ApproveRejectButtonPair extends Component<ApproveRejectButtonPairProps, unknown> {
    constructor(props: ApproveRejectButtonPairProps) {
        super(props);
    }

    render() {
        return (
            <div className="d-flex flex-column align-items-center">
                <ul className="pagination">
                    <li className="page-item mr-4" style={{width: 100}}>
                        <button
                            className="w-100 btn btn-success"
                            onClick={() => this.props.onApprove()}>
                            Approve
                        </button>
                    </li>
                    <li className="page-item" style={{width: 100}}>
                        <button
                            className="w-100 btn btn-danger"
                            onClick={() => this.props.onReject()}>
                            Reject
                        </button>
                    </li>
                </ul>
            </div>
        );
    }
}
