import React, {FC, useContext} from "react";
import {Link} from "react-router-dom";
import {ROUTES} from "../../routes";
import {AuthenticationContext, AuthenticationState} from "../../contexts";


// eslint complained here, known issue with having to redundantly specify the type twice
export const Navbar: FC = () => {
    const {logout, isEditor} = useContext<AuthenticationState>(AuthenticationContext);

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                    <Link className="nav-link" to={ROUTES.dashboard}>Dashboard</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to={ROUTES.article}>Create article</Link>
                </li>
                <li className="nav-item">
                    {/*Defaults to id: 1, to prove functionality as the use-case in not really reachable based on req.*/}
                    <Link className="nav-link" to={ROUTES.articleDetail.replace(':articleId', '1')}>View Article (id:
                        1)</Link>
                </li>
                {
                    isEditor ? (
                        <>
                            <li className="nav-item">
                                <Link className="nav-link" to={ROUTES.articleApproval}>Article Approval</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to={ROUTES.articlesEdited}>Articles Edited</Link>
                            </li>
                        </>
                    )
                    :
                    null
                }
            </ul>
            {
                logout ? <a className="navbar-brand" onClick={() => logout()}>Logout</a> : null
            }
        </nav>
    );
};
