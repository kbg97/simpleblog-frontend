import React, {FC} from "react";
import {Article} from "types";
import {ApproveRejectButtonPair} from "../ApproveRejectButtonPair";
import {StatusBadge} from "../StatusBadge";

type ArticleListProps = {
    rows?: Article[]
    onApprove?: (id: number) => void;
    onReject?: (id: number) => void;
};

// eslint complained here, known issue with having to redundantly specify the type twice
export const ArticleList: FC<ArticleListProps> = ({ rows, onApprove, onReject }: ArticleListProps) => (
    <ul className="list-group">
        {
            rows?.map(article => (
                <li
                    key={article.id}
                    className="list-group-item d-flex flex-row align-items-center justify-content-between">
                    <span>{article.id}. {article.title}</span>
                    {onApprove && onReject ?
                        (
                            <ApproveRejectButtonPair
                                onApprove={() => onApprove(article.id)}
                                onReject={() => onReject(article.id)}/>
                        )
                        :
                        <StatusBadge status={article.status}/>
                    }
                </li>
            ))
        }
    </ul>
);
