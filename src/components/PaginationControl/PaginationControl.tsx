import React, {Component} from "react";

type PaginationControlProps = {
    onNext: () => void;
    onPrev: () => void;
    isNextEnabled: boolean;
    isPrevEnabled: boolean;
    hasResults: boolean;
    totalCount: number;
    pageSize: number;
};

type PaginationControlState = {
    currentPageNumber: number;
};

export class PaginationControl extends Component<PaginationControlProps, PaginationControlState> {
    constructor(props: PaginationControlProps) {
        super(props);

        this.state = {
            currentPageNumber: Math.min(1, Math.ceil(this.props.totalCount / this.props.pageSize))
        }
    }

    shouldComponentUpdate(nextProps: Readonly<PaginationControlProps>): boolean {
        if (nextProps.totalCount !== this.props.totalCount) {
            this.setState({currentPageNumber: Math.min(1, Math.ceil(nextProps.totalCount / this.props.pageSize))})
        }
        return true;
    }

    onNext = (): void => {
        if (this.props.isNextEnabled) {
            this.props.onNext();
            this.setState({currentPageNumber: this.state.currentPageNumber + 1})
        }
    }

    onPrev = (): void => {
        if (this.props.isPrevEnabled) {
            this.props.onPrev();
            this.setState({currentPageNumber: this.state.currentPageNumber - 1})
        }
    }

    render() {
        return (
            <div className="d-flex flex-column align-items-center">
                <p className="text-info">Page: {this.state.currentPageNumber} / {Math.ceil(this.props.totalCount / this.props.pageSize)}</p>
                <ul className="pagination">
                    <li className="page-item mr-4" style={{width: 100}}>
                        <button
                            className={`w-100 btn btn-secondary ${!this.props.isPrevEnabled ? ' disabled' : ''}`}
                            onClick={() => this.onPrev()}>
                            Previous
                        </button>
                    </li>
                    <li className="page-item" style={{width: 100}}>
                        <button
                            className={`w-100 btn btn-primary ${!this.props.isNextEnabled ? ' disabled' : ''}`}
                            onClick={() => this.onNext()}>
                            Next
                        </button>
                    </li>
                </ul>
            </div>
        );
    }
}
