import React, {FC} from "react";
import {DashboardWriter} from "types";

type DashboardTableProps = {
    rows?: DashboardWriter[]
};

// eslint complained here, known issue with having to redundantly specify the type twice
export const DashboardTable: FC<DashboardTableProps> = ({ rows }: DashboardTableProps) => (
    <table className="table mt-3 mb-5">
        <thead style={{background: 'whitesmoke'}}>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Writer</th>
            <th scope="col">Total Articles Written</th>
            <th scope="col">Total Articles Written Last 30</th>
        </tr>
        </thead>
        <tbody>
        {
            rows?.map(writer => (
                <tr key={writer.id}>
                    <th scope="row">{writer.id}</th>
                    <td>{writer.name}</td>
                    <td>{writer.total_articles}</td>
                    <td>{writer.total_articles_last_30}</td>
                </tr>
            ))
        }
        </tbody>
    </table>
);
