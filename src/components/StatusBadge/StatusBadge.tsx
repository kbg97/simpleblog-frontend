import React, {FC} from "react";
import {ArticleStatus} from "../../types";

type StatusBadgeProps = {
    status?: ArticleStatus;
}

export const StatusBadge: FC<StatusBadgeProps> = ({status}: StatusBadgeProps) => {
    switch (status) {
        case ArticleStatus.Pending:
            return <span className="badge badge-secondary">{status}</span>
        case ArticleStatus.Accepted:
            return <span className="badge badge-success">{status}</span>
        case ArticleStatus.Rejected:
            return <span className="badge badge-danger">{status}</span>
        default:
            return null;
    }
};
