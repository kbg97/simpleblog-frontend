export const ROUTES = {
    dashboard: '/dashboard',
    login: '/login',
    error404: '/404',
    article: '/article',
    articleDetail: '/article/:articleId',
    articleApproval: '/article-approval',
    articlesEdited: '/articles-edited',
};
